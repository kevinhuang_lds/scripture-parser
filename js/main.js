var aryExcludes = ["in","out", "at", "is", "were", "was", "am", "are", "over", "and", "not", "would", "a", "for", "of"];
var sentences = [];
var objLines = [] ;
var storageKey = "bom_voc";
var objStorage ;

$(document).ready(function() {
  //先從 local storage 中取出資料
  var storageContent = localStorage.getItem(storageKey);
  if (storageContent) {
    objStorage = JSON.parse(storageContent);
  }
  else {
    objStorage = {} ;
  }
});

var findChn = function(engKey) {
  var chn = objStorage[engKey];
  return (chn ? chn : ''); 
}

var showInputArea = function() {
  if ($('#rawText').height() == 200) {
    hideInputArea();
    $('btnShow').text("Show Area");
  }
  else {
    $('#rawText').height(200);
    $('btnShow').text("Hide Area");
  }
};

var hideInputArea = function() {
  $('#rawText').height(0);
};


var parseSentences = function() {

  hideInputArea();
  $('#lstSentences').html("");
  objLines = [] ;

  //取得原始資料
  var rawdata = document.getElementById('rawText').value;
  console.log(rawdata);

  //根據斷行符號切成一句一句
  sentences  = rawdata.split(/\n/);
  //對於每一個句子
  $(sentences).each(function(index, sentence) {
    var lineNo = index + 1 ;
    objLine = {"no" : lineNo, "sentence" : sentence, words : []};
    objLines.push(objLine);
  });

  //show all sentences :
  $(objLines).each(function(index, line) {
    var html = ["<div class='sentence' id='" , index , "'>", line.no, ". ", line.sentence.substr(0, 40) + "...", "</div>"].join("");
    $(html).appendTo('#lstSentences').click(function() {
      //顯示句子內容
      $('#divSentence').html( [line.no, ". ", line.sentence].join(""));
      $('div.sentence').removeClass('selected');
      $(this).addClass('selected');

      //Clean UI
      $('#divResult').html("");
      $('#copyarea').val('');
      //解析並顯示單字
      parseText(line);
    }).mouseover(function() {
      $(this).addClass("mi");
    }).mouseout(function() {
      $(this).removeClass("mi");
    });
  });
};

var parseText = function(objLine) {

  $('#wordlist').html("");
  //先檢查是否已經翻譯過

  var words = objLine.sentence.split(/ +/); //根據空白切出一個一個單字
  var result = [] ;
  var dicResult = {};
  $(words).each(function(index, item) {
    if ($.inArray(item, aryExcludes) == -1) {
      var voc_eng = item.replace(",","").replace(".","").replace(";","") ;
      if (!dicResult[voc_eng]) {
        console.log(voc_eng);
        dicResult[voc_eng] = true ;
        // result.push(item);
        var html = [
                    "<div class='row'>" ,
                       "<span class='voc_no'>", (index + 1), ". </span>",
                       "<span class='voc voc_e' id='e_", (index + 1), "' contenteditable='true'>", voc_eng , "</span>",
                       "<span class='voc voc_c' id='c_e_", (index + 1), "' contenteditable='true'>", findChn(voc_eng), "</span>",
                       
                    "</div>"].join("");
        $(html).appendTo('#wordlist');
      }
    }
  });

  //如果修改英文，要自動比對找出符合的中文
  $('span.voc').on('DOMSubtreeModified',function(){
    var id = $(this).attr('id');
    var voc = $(this).html();
    if (objStorage[voc]) {
      $('#c_' + id).html(findChn(voc));
    }
  })
};

//Save to Local Storage.
var saveToLocal = function() {
  $('#divResult').html("");
  var copiedText = "";
  var resultTable = $("<table></table>").appendTo('#divResult');
  $('span.voc').each(function() {
    var id = $(this).attr('id') ;
    var eng = $(this).html();
    var chn = $('#c_' + id).html();
    if (eng && chn) {
      objStorage[eng] = chn ;
      copiedText += [eng, "\t", chn, "\n"].join("");
      var rowHtml = ["<tr><td>", eng, "</td><td>", chn, "</td></tr>"].join("");
      $(rowHtml).appendTo(resultTable );
    }
  });


  $('#copyarea').val(copiedText);

  //save to local
  localStorage.setItem(storageKey, JSON.stringify(objStorage));

  //show result
  copyToClipboard();
};

var copyToClipboard = function() {
  var target = document.getElementById("copyarea");
  target.focus();
  target.setSelectionRange(0, target.value.length);

  document.execCommand("copy");
};


